#pragma once

#include"Mesh.h"
#include"Texture.h"
#include"Shader.h"
#include"Material.h"

class Model {
private:
	Material* material;
	Texture* overrideTextureDiffuse;
	Texture* overrideTextureSpecular;
	std::vector<Mesh*> meshes;
	glm::vec3 position;

	void updateUniforms() {

	}

public:
	Model(
		glm::vec3 position,
		Material* material,
		Texture* orTexDif,
		Texture* orTexSpec,
		std::vector<Mesh*>& meshes
	) {
		this->position = position;
		this->material = material;
		this->overrideTextureDiffuse = orTexDif;
		this->overrideTextureSpecular = orTexSpec;

		for (auto* i : meshes) {
			this->meshes.push_back(new Mesh(*i));
		}

		for (auto& i : this->meshes) {
			i->move(this->position);
			i->setOrigin(this->position);
		}
	}

	~Model() {
		for (auto*& i : this->meshes)
			delete i;
	}

	//Functions
	void rotate(const glm::vec3 rotation) {
		for (auto& i : this->meshes)
			i->rotate(rotation);
	}

	void setRotate(const float rotation) {
		for (auto& i : this->meshes)
			i->setRotate(rotation);
	}

	void move(const float& dt, const glm::vec3 amount) {
		glm::vec3 diff = glm::vec3(amount.x * dt * 3.f, amount.y * dt * 3.f, amount.z * dt * 3.f);
		this->position += glm::vec3(this->position.x + (amount.x * dt * 3.f), this->position.y + (amount.y * dt * 3.f), this->position.z + (amount.z * dt * 3.f));
		for (auto& i : this->meshes) {
			i->move(diff);
			i->setOrigin(diff);
		}
	}

	glm::vec3 getPosition() {
		return this->meshes.at(0)->getPositionz();
	}

	void update() {

	}

	void render(Shader* shader) {
		//Update the uniforms
		this->updateUniforms();

		//Update uniforms
		this->material->sendToShader(*shader);

		//Use a program
		shader->use();

		//Activate texture
		this->overrideTextureDiffuse->bind(0);
		this->overrideTextureSpecular->bind(1);

		//Draw
		for (auto& i : this->meshes)
			i->render(shader);
	}
};