#include "Game.h"

int main() {
	Game game("Final Project", 1920, 1080, 4, 4, false);

	while (!game.getWindowShouldClose() && !game.isWin) {
		game.update();
		game.render();
	}

	return 0;
}